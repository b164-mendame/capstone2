const express = require("express");
const mongoose = require("mongoose");
require('dotenv').config();
const cors = require("cors");
//dotenv is to hide your MonggoDB connection string


//Routes

const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes")


//server
const app = express();

/* 

-> List of people who can access

const whitelist = ['http://example.com', 'http://localhost:400']


-> Cors - Allows a specific link that can access our backend.

const corsOptions = {

    origin: function

}

*/


//Allows all resources/origins to access our backend application

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}));



//http:localhost:5000/api/users
app.use("/api/users", userRoutes)

//http:localhost:4000/api/courses
app.use("/api/products", productRoutes);


//Connect to our MongoDB connection

mongoose.connect(process.env.DB_CONNECTION, {
	useNewUrlParser:true,
	useUnifiedTopology:true
})

mongoose.connection.once('open',() => console.log('Now connected to MongoDB Atlas'));

app.listen(process.env.PORT,() => {console.log(`API is now online on port ${process.env.PORT}`)})

//.env is to hide the port