const User = require("../models/Users")
const Product = require("../models/Products")
const auth = require("../auth")
//encrypted password
const bcrypt = require("bcrypt")





//User Registration
/*
Steps:
1. Create a new User object 
2. Make sure that the password is encrypted
3. Save the new User to the database
*/
/*
Function parameters from routes to controllers

Routes (argument)			Controllers(parameter)
registerUser(req.body, req.params) = >  (reqBody, params)
req.body.firstName => reqBody.firstName
*/


module.exports.registerUser = (reqBody) => {

	//Creates a new User Object
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		//10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	//Saves the created object to our database
	return newUser.save().then((user, error) => {
		//User registration failed
		if(error) {
			return false;
		} else {
			//User Registration is success
			return true
		}
	})

}


//User Authentication

/*
Steps:
1. Check the database if the user email exists
2. Compare the password provided in the login form with the password stored in the database.
3. Generate/return a JSON web token if the user is successfully logged in and return false if not

*/

module.exports.loginUser = (reqBody) => {

	//findOne will return the first record in the collection that matches the search criteria

	return User.findOne({ email: reqBody.email }).then(result => {
		//User does not exist
		if(result == null){
			return false;
		} else {
			//User exists

			//The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false".
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			//if the password match
			if(isPasswordCorrect){
				//Generate an access token
				return { accessToken : auth.createAccessToken(result.toObject())}
			} else {
				//Password does not match
				return false;
			}

		};
	});
};





//Update User Access


/*
1. Create a variable which will contain the information retrieved from the request body.
2. Find and update the course using the course ID.
*/

//NOTE: Figure out how the two arguments are used

module.exports.updateAccess = (userId, reqBody) => {
	//specify the properties of the doc to be updated
	let updatedUser = {
		isAdmin: reqBody.isAdmin
	};

	//findByIdAndUpdate(id, updatesToBeApplied)
	return User.findByIdAndUpdate(userId, updatedUser).then((product, error) => {
		//course not updated
		if(error) {
			return false;
		}else {
			//user updated successfully
			return true;
		}
	})
}


//CREATE AN ORDER (USER)
/*
Steps:
1. Find the document in the database using the user's ID
2. Add the productID to the user's enrollment array using the push method.
3. Add the userId to the course's enrollees arrays.
4. Save the document

*/


//Async and await - allow the processes to wait for each other


module.exports.buyProduct = async (data) => {

	//Add the productId to the orders array of the user

	let isUserUpdated = await User.findById(data.userId).then( user => {
		//push the productId to orders property

		user.order.push({productId: data.productId});

		//save
		return user.save().then((user, error) => {
			if(error) {
				return false;
			}else {
				return true
			}
		})
	});


	let isProductUpdated = await Product.findById(data.productId).then(product => {
		//add the userrId in the product's database(ordersperproduct)
		product.ordersPerProduct.push({userId: data.userId});

		return product.save().then((product, error) => {
			if(error) {
				return false;
			}else {
				return true;
			}
		})
	});


	//Validation
	if(isUserUpdated && isProductUpdated){
		//user purchase successful
		return true;
	}else {
		//user enrollment failed
		return false;
	}

};



//RETRIEVE AUTHENTICATED USER ORDER

module.exports.getOrder = (userId) => {

	return User.findById(userId).then(result => {
		
		return result.order //return specific property from the userId provided.
	})
}


//RETRIEVE ALL ORDERS (ADMIN)

module.exports.getUserOrders = () => {
	return User.find({isAdmin:false}).then( result => {
		
        result.password = "";

		return result;

	})
}
