const Product = require("../models/Products");


//Create a new product
/*
Steps:
1. Create a new product object
2. Save to the database
3. error handling
*/


module.exports.addProduct = (reqBody) => {
	console.log(reqBody);

	//Create a new object
	let newProduct = new Product({
		name: reqBody.product.name,
		description: reqBody.product.description,
		price: reqBody.product.price
	});

	//Saves the created object to our database
	return newProduct.save().then((course, error) => {
		//Product creation failed
		if(error) {
			return false;
		} else {
			//Product Creation successful
			return true;
		}
	})

}



//Retrieving all Active products

module.exports.getAllActive = () => {
	return Product.find({ isActive: true }).then(result => {
		return result;
	})
}


//RETRIEVE A SPECIFIC PRODUCT

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {
		return result;
	})
}

//UPDATE A PRODUCT

module.exports.updateProduct = (productId, reqBody) => {
	//specify the properties of the doc to be updated
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive
	};

	//findByIdAndUpdate(id, updatesToBeApplied)
	return Product.findByIdAndUpdate(productId, updatedProduct).then((course, error) => {
		//course not updated
		if(error) {
			return false;
		}else {
			//course updated successfully
			return true;
		}
	})

}


//ARCHIVE A PRODUCT

module.exports.archiveProduct = (reqParams) => {
	//object

	let updateActiveField = {
		isActive: false
	};

	//findByIdAndUpdate(id, updatesToBeApplied)

	return Product.findByIdAndUpdate(reqParams, updateActiveField).then((course, error) => {

		if(error) {
			//if archive is not successful
			return false;
		}else {
			//if archive is successful
			return true;
		}
	})

}


