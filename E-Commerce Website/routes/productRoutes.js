const express = require("express");
const router = express.Router();
const ProductController = require("../controllers/productControllers");
const auth = require('../auth')



//CREATE PRODUCT (ADMIN ONLY)
router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	console.log(data.isAdmin)

	if(data.isAdmin) {
		ProductController.addProduct(data).then(result => res.send(result));
	} else {
		res.send({ auth: "You're not an admin"})
	}
})


//RETRIEVE ALL ACTIVE PRODUCTS

router.get("/active", (req, res) => {
	ProductController.getAllActive().then(result => res.send(result))
});



//RETRIEVE SPECIFIC PRODUCT

router.get("/:productId", (req, res) => {
	console.log(req.params.courseId);

	ProductController.getProduct(req.params.productId).then(result => res.send(result))   //productID aka params is flexible. You can put anything there.
})


//UPDATE A PRODUCT

router.put("/:productId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
})



//ARCHIVING A PRODUCT
//1. Create a route for archiving a product. The route must use JWT authentication and obtain the product ID from the url. Change true to false.

router.put("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		ProductController.archiveProduct(req.params.productId).then(result => res.send(result));
	} else {
		res.send(false)
	}
})

module.exports = router;