const express = require("express")
const router = express.Router();
const auth = require("../auth")
const UserController = require("../controllers/userControllers")


//User Registration

router.post("/register", (req, res) =>{
	UserController.registerUser(req.body).then(result => res.send(result));
})



//User Authentication(login)

router.post("/login", (req, res) => {
	UserController.loginUser(req.body).then(result => res.send(result));
});



//Set User As Admin (Admins Only)

router.put("/:userId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		UserController.updateAccess(req.params.userId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
})


//CREATE ORDER (USER)

router.post("/buy", auth.verify, (req, res) => {
	let data = {
	    userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId,
		//Ask how to add name of product
	}

	UserController.buyProduct(data).then(result => res.send(result));
	
	
})


//RETRIEVE USER ORDER

router.get("/userOrder", auth.verify, (req, res) => {
	//decode() to retrieve the user information from the token passing the "token" from te request headers as an argument

	const userId = auth.decode(req.headers.authorization);

	UserController.getOrder(userId.id).then(result => res.send(result))
} )


//RETRIEVE ALL ORDERS

	router.get("/allOrders", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		UserController.getUserOrders().then(result => res.send(result));
	} else {
		res.send(false)
	}
})



module.exports = router;